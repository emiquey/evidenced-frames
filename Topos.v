Require Import Cartesian.


Module Type Topos.
  Include CartesianClosedCategory.

  Parameter oprop : Object.
  Parameter opropv : ObjectV oprop.
  Parameter mtrue : Morphism ounit oprop.
  Parameter mtruev : MorphismV mtrue.

  Parameter osub : forall {o : Object}, Morphism o oprop -> Object.
  Parameter osubv : forall o : Object, forall mp : Morphism o oprop, ObjectV o -> MorphismV mp -> ObjectV (osub mp).
  Parameter msubi : forall {o1 o2 : Object}, forall mp : Morphism o2 oprop, Morphism o1 o2 -> Morphism o1 (osub mp).
  Parameter msubiv : forall o1 o2 : Object, forall mp : Morphism o2 oprop, forall m : Morphism o1 o2, ObjectV o1 -> ObjectV o2 -> MorphismV mp -> MorphismV m -> MorphismE (mcomp (muniti o1) mtrue) (mcomp m mp) -> MorphismV (msubi mp m).
  Parameter msube : forall {o : Object}, forall mp : Morphism o oprop, Morphism (osub mp) o.
  Parameter msubev : forall o : Object, forall mp : Morphism o oprop, MorphismV (msube mp).

  Parameter msubie : forall o1 o2 : Object, forall mp : Morphism o2 oprop, forall m : Morphism o1 o2, forall ms : Morphism o1 (osub mp), ObjectV o1 -> ObjectV o2 -> MorphismV mp -> MorphismV ms -> MorphismE (mcomp (muniti o1) mtrue) (mcomp m mp) -> MorphismE m (mcomp ms (msube mp)) -> MorphismE ms (msubi mp m).
  Parameter msubee : forall o : Object, forall mp : Morphism o oprop, ObjectV o -> MorphismV mp -> MorphismE (mcomp (muniti (osub mp)) mtrue) (mcomp (msube mp) mp).
Require Import Program.
Require Import EF.Categories.
Require Import  Coq.Relations.Relation_Definitions.
Require Import Coq.Init.Datatypes.
Require Import EF.Triposes.
Obligation Tactic := idtac.





(* Topos :
    1- Finitely Complete: 
        1.1- terminal object 
        1.2- all binary products 
        1.3- all equalizers

    2- Finitely Co-Complete:
      2.1- inital object
      2.2- all binary co-products
      2.3- all co-equalizers

    3- Has Exponential objects
    4- Has a subobject classifier
  *)

  (* 1.1- Terminal Object def.
  terminal : exists (t : Object), forall (x : Object),
             exists ( f : (Morphism x t)), forall (g : Morphism x t), f = g;

  (* 2.1 Initiall Object def. *)
  initial: exists (i : Object), forall (x : Object),
           exists ( f : (Morphism i x)), forall (g : Morphism i x), f = g;

  (* 1.2 Binary products def. *)
  binary_products : forall (x1 x2 : Object), exists (x : Object) (p1 : Morphism x x1) (p2 : Morphism x x2),
                    forall (x' : Object) (p1' : Morphism x' x1) (p2' : Morphism x' x2),
                    exists (u : Morphism x' x), ccomp u p1 = p1' /\ ccomp u p2 = p2';
  (* 2.2 Binary co-products def. *)
  binary_coproducts : forall (x1 x2 : Object), exists (x : Object) (i1 : Morphism x1 x) (i2 : Morphism x2 x),
                    forall (x' : Object) (i1' : Morphism x1 x') (i2' : Morphism x2 x'),
                    exists (u : Morphism x x'), ccomp i1 u = i1' /\ ccomp i2 u = i2';

  (* 1.3 all equalizers def. 
  equalizer : forall (a b : Object) (f g : Morphism a b), exists (eq : Object) (e : Morphism eq a),
              ccomp e f = ccomp e g /\ forall (eq' : Object) ( e' : Morphism eq' a),
              exists (k : Morphism eq' eq), ccomp k e = e' /\ forall (k' : Morphism eq' eq),
              ccomp k' e = e' -> k' = k;

  (* 2.3 all co-equalizers def. *)
  coequalizer : forall (a b : Object) (f g : Morphism a b), exists (eq : Object) (e : Morphism b eq),
              ccomp f e = ccomp g e /\ forall (eq' : Object) ( e' : Morphism b eq'),
              exists (k : Morphism eq' eq), ccomp k e = e';*)

  (* Binary products def. 
  pullback : forall (a b c : Object) (f : Morphism a c) (g : Morphism b c),
              exists (x : Object) (f' : Morphism x a) (g' : Morphism x b),ccomp f' f = ccomp g' g /\
              forall (x' : Object) (f'' : Morphism x' a) (g'' : Morphism x' b), 
              ccomp f'' f = ccomp g'' g -> exists (k : Morphism x' x), 
              forall (k' : (Morphism x' x)), k = k' /\ ccomp k f' = f'' /\ ccomp k g' = g'';*)

  *)




(*****************************************************
********************************************************)
(*



Record Category : Class := category {
  Object : Type;
  Morphism : Object -> Object -> Type;
  Monomorphism : forall {a b : Object}, Morphism a b -> Prop;
  cid : forall o : Object, Morphism o o;
  ccomp : forall {o1 o2 o3 : Object}, Morphism o1 o2 -> Morphism o2 o3 -> Morphism o1 o3;
  cassoc : forall {o1 o2 o3 o4 : Object} (f: Morphism o1 o2) (g : Morphism o2 o3) (h : Morphism o3 o4),
           ccomp (ccomp f g) h = ccomp f (ccomp g h);
}.

Lemma cid_ : forall (C : Category) (o: Object C ), Morphism C o o.
intros C o.  apply cid. Qed.

Lemma ccomp_ : forall (C : Category) (o1 o2 o3 : Object C), Morphism C o1 o2 -> Morphism C o2 o3 -> Morphism C o1 o3.
intros C o1 o2 o3.
apply ccomp.
Qed.

Lemma cassoc_ : forall {C :Category} {o1 o2 o3 o4 : Object C}
                (f : Morphism C o1 o2)
                (g : Morphism C o2 o3)
                (h : Morphism C o3 o4),
                ccomp C (ccomp C f g) h = ccomp C f (ccomp C g h).
intros C o1 o2 o3 o4. apply cassoc. Qed.






Record Product (C : Category) (c d : Object C) : Type :={
  product : Object C;
  Pi_1 : Morphism C product c;
  Pi_2 : Morphism C product d;
  Prod_morph_ex : forall (p' : Object C) (r1 : Morphism C p' c) (r2 : Morphism C p' d), Morphism C p' product;
  Prod_morph_com_1 : forall (p' : Object C) (r1 : Morphism C p' c) (r2 : Morphism C p' d),
      ccomp C (Prod_morph_ex p' r1 r2) Pi_1  = r1;
  Prod_morph_com_2 : forall (p' : Object C) (r1 : Morphism C p' c) (r2 : Morphism C p' d),
      ccomp C (Prod_morph_ex p' r1 r2) Pi_2  = r2;
  Prod_morph_unique :
    forall (p' : Object C) (r1 : Morphism C p' c) (r2 : Morphism C p' d) (f g : Morphism C p' product),
      ccomp C f Pi_1 = r1 ->
      ccomp C f Pi_2 = r2 ->
      ccomp C g Pi_1 = r1 ->
      ccomp C g Pi_2 = r2 -> f = g;
}.



Record CoProduct (C : Category) (c d : Object C) : Type :={
  coproduct : Object C;
  i_1 : Morphism C c coproduct;
  i_2 : Morphism C d coproduct;
  coProd_morph_ex : forall (p' : Object C) (r1 : Morphism C  c p') (r2 : Morphism C d p'), Morphism C coproduct p';
  coProd_morph_com_1 : forall (p' : Object C) (r1 : Morphism C c p') (r2 : Morphism C d p'),
      ccomp C i_1 (coProd_morph_ex p' r1 r2)   = r1;
  coProd_morph_com_2 : forall (p' : Object C) (r1 : Morphism C c p') (r2 : Morphism C d p'),
      ccomp C i_2 (coProd_morph_ex p' r1 r2)  = r2;
  cpProd_morph_unique :
    forall (p' : Object C) (r1 : Morphism C c p') (r2 : Morphism C d p') (f g : Morphism C coproduct p'),
      ccomp C i_1 f = r1 ->
      ccomp C i_2 f = r2 ->
      ccomp C i_1 g = r1 ->
      ccomp C i_2 g = r2 -> f = g;
}.

Record Terminal_Obj (C : Category): Type := {
  T : Object C;
  T_all_morph : forall (x : Object C), Morphism C x T;
  T_unique_morph : forall {x: Object C} (f g : Morphism C x T), f = g;
}.



Record Initial_Obj (C : Category): Type := {
  I: Object C;
  I_all_morph : forall (x : Object C), Morphism C I x;
  I_unique_morph : forall {x: Object C} (f g : Morphism C I x), f = g;
}.


Record Equalizer (C : Category) (a b : Object C) (f g: Morphism C a b) : Type :={
  equalizer : Object C;
  equalizer_morph : Morphism C equalizer a;
  equalizer_morph_com : ccomp C equalizer_morph f = ccomp C equalizer_morph g;
  
  equalizer_morph_ex : forall (e' : Object C) (eqm : Morphism C e' a),
    ccomp C eqm f = ccomp C eqm g -> Morphism C e' equalizer;
  equalizer_morph_ex_com : forall (e' : Object C) (eqm : Morphism C e' a) 
    (eqmc : ccomp C eqm f  = ccomp C eqm g), ccomp C (equalizer_morph_ex e' eqm eqmc) equalizer_morph = eqm;
}.

Record CoEqualizer (C : Category) (a b : Object C) (f g: Morphism C a b) : Type :={
  coequalizer : Object C;
  coequalizer_morph : Morphism C b coequalizer;
  coequalizer_morph_com : ccomp C f coequalizer_morph = ccomp C g coequalizer_morph;
  
  coequalizer_morph_ex : forall (e' : Object C) (eqm : Morphism C b e'),
    ccomp C f eqm = ccomp C g eqm -> Morphism C coequalizer e';

  coequalizer_morph_ex_com : forall (e' : Object C) (eqm : Morphism C b e') 
    (eqmc : ccomp C f eqm  = ccomp C g eqm), ccomp C  coequalizer_morph (coequalizer_morph_ex e' eqm eqmc) = eqm;
}.


Record PullBack (C : Category) (a b x : Object C) (f: Morphism C a x) (g : Morphism C b x) : Type := {
  pullback : Object C;
  pullback_morph_1 : Morphism C pullback a;
  pullback_morph_2 : Morphism C pullback b;
  pullback_morph_com : ccomp C pullback_morph_1 f  = ccomp C pullback_morph_2 g;
  pullback_morph_ex (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b) :
        ccomp C pm1 f = ccomp C pm2 g -> Morphism C p' pullback;
  pullback_morph_ex_com_1 (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b)
                              (pmc : ccomp C pm1 f = ccomp C pm2 g)
      :
        ccomp C (pullback_morph_ex p' pm1 pm2 pmc) pullback_morph_1 = pm1;
  pullback_morph_ex_com_2 (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b)
                              (pmc : ccomp C pm1 f = ccomp C pm2 g)
      :
        ccomp C (pullback_morph_ex p' pm1 pm2 pmc) pullback_morph_2 = pm2;

  pullback_morph_ex_unique
        (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b)
        (pmc : ccomp C pm1 f = ccomp C pm2 g) (u u' : Morphism C p' pullback) :
        ccomp C u pullback_morph_1 = pm1 ->
        ccomp C u pullback_morph_2  = pm2 ->
        ccomp C u' pullback_morph_1 = pm1 ->
        ccomp C u' pullback_morph_2 = pm2 -> u = u';

}.

Record is_PullBack (C : Category) (a b x pb: Object C) 
                    (f: Morphism C a x) (g : Morphism C b x) 
                    (p1: Morphism C pb a) (p2 : Morphism C pb b) : Type :={
  is_pullback_morph_com : ccomp C p1 f  = ccomp C p2 g;

  is_pullback_morph_ex (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b) :
    ccomp C pm1 f  = ccomp C pm2 g -> Morphism C p' pb;

  is_pullback_morph_ex_com_1 (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b)
                             (pmc : ccomp C pm1 f  = ccomp C pm2 g)
  :
    ccomp C (is_pullback_morph_ex p' pm1 pm2 pmc) p1 = pm1;

  is_pullback_morph_ex_com_2 (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b)
                             (pmc : ccomp C pm1 f  = ccomp C pm2 g)
  :
    ccomp C (is_pullback_morph_ex p' pm1 pm2 pmc) p2 = pm2;

  is_pullback_morph_ex_unique
    (p' : Object C) (pm1 : Morphism C p' a) (pm2 : Morphism C p' b)
    (pmc : ccomp C pm1 f  = ccomp C pm2 g) (u u' : Morphism C p' pb) :
    ccomp C u p1 = pm1 ->
    ccomp C u p2 = pm2 ->
    ccomp C u' p1 = pm1 ->
    ccomp C u' p2 = pm2 -> u = u';
}.


Definition Has_product (C : Category) := forall {a b : Object C}, Product C a b.
Definition Has_coproduct (C : Category) := forall {a b : Object C}, CoProduct C a b.
Definition Has_terminal (C : Category) := Terminal_Obj C.
Definition Has_initial (C : Category) := Initial_Obj C.
Definition Has_equalizer (C:Category) := forall {a b : Object C} {f g : Morphism C a b},
    Equalizer C a b f g.
Definition Has_coequalizer (C:Category) := forall {a b : Object C} {f g : Morphism C a b},
    CoEqualizer C a b f g.
Definition Has_pullback (C:Category) := forall {a b x : Object C} {f : Morphism C a x} {g : Morphism C b x},
            PullBack C a b x f g.


(*

Record SubObject_Classifier (C : Category) (Ter : Terminal_Obj C) : Type := {
  SOC : Object C;
  True : Morphism C SOC (T Ter);
}.
*)


Record Topos : Type := topos {
  (* First We add the normal category defs. *)
  Topos_Cat : Category ;
  Topos_has_prod : Has_product Topos_Cat; 
  Topos_has_terminal : Has_terminal Topos_Cat;
  Topos_has_initial : Has_initial Topos_Cat;
  Topos_has_equalizer : Has_equalizer Topos_Cat;
  Topos_has_coequalizer : Has_coequalizer Topos_Cat;
  Topos_has_pullback : Has_pullback Topos_Cat;
  
}.

*)


(*****************************************************
********************************************************)




Record Topos : Type := topos {
  (* First We add the normal category defs. *)
  Object : Type;

  Terminal : Object;

  Initial : Object;

  Morphism : Object -> Object -> Type;  

  Is_monomorphism : forall {a b :Object}, Morphism a b -> Prop;

  Is_unique_morphism : forall {o1 o2: Object}, Morphism o1 o2 -> Prop;

  Product : Object -> Object -> Object;

  Coproduct : Object -> Object -> Object;

  Is_terminal : Object -> Prop;

  Is_initial : Object -> Prop;

  Exponential : Object -> Object -> Object;

  cid : forall o : Object, Morphism o o;

  ccomp : forall {o1 o2 o3 : Object}, Morphism o1 o2 -> Morphism o2 o3 -> Morphism o1 o3;

  cterminal : exists (t : Object), Is_terminal t;

  cinitial : exists (i : Object), Is_initial i;

  cprod : forall {a b : Object}, exists (p : Object), p = Product a b;

  cequalizer : forall (a b : Object) (f g : Morphism a b), exists (eq : Object) (e : Morphism eq a),
               ccomp e f = ccomp e g /\ forall (eq' : Object) (e' : Morphism eq' a),
               ccomp e' f = ccomp e' g -> exists (k : Morphism eq' eq) , ccomp k e = e' /\ Is_unique_morphism k;

  ccoequalizer : forall (a b : Object) (f g : Morphism a b), exists (c : Object) (e : Morphism b c),
               ccomp f e = ccomp g e /\ forall (c' : Object) (e' : Morphism b c'),
               ccomp f e' = ccomp g e' -> exists (k : Morphism c c') , ccomp e k = e' /\ Is_unique_morphism k;

  cexponential : forall (a b : Object), exists (bea : Object), bea = Exponential a b;

  csubobjclassifier : exists (omega : Object), forall (a b : Object) (f : Morphism a b),
                      Is_monomorphism f ->  exists (chai_f : Morphism b omega), Is_unique_morphism chai_f;

}.
(*****************************************************
********************************************************)

Record Pitts_obj : Type := p_obj
{
  Obj : Type;
  E : Obj -> Obj -> Prop;
  E_sym : forall (x y : Obj), E x y -> E y x;
  E_tran : forall (x y z : Obj), E x y -> E y z -> E x z;
}.

Record POMorphism (o1 o2 : Pitts_obj) : Type := po_morphism {
  F : Obj o1 -> Obj o2 -> Prop;
  
  Fstrict: forall (x: Obj o1) (y : Obj o2), F x y  -> E o1 x x /\ E o2 y y;

  Fcongruent : forall (x x' : Obj o1) (y y' : Obj o2), F x y /\ E o1 x x' /\ E o2 y y' -> F x' y';

  Funique: forall (x : Obj o1) (y y' : Obj o2), F x y /\ F x y' -> E o2 y y';

  Ftotal : forall (x : Obj o1), E o1 x x -> exists (y : Obj o2), F x y;

  
}.


Program Definition TTT_Object {A : Tripos} ( X : Omega A)  : Pitts_obj
:= p_obj X (LEq (Pred A X)).
















