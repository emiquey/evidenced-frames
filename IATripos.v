Require Import Program.
Require Import EF.Categories.
Require Import EF.Triposes.
Require Import EF.EvidencedFrames.
Require Import EF.ImplicativeAlgebras.


Obligation Tactic := idtac.

Section IATripos.
  Context `{IA:ImplicativeAlgebra}.
  Infix "↦" := arrow (at level 60, right associativity):ia_scope.

  (** *** Structure *)
  (* Definition X:=IASet IA. *)
  (* Definition sep:IASet IA-> Prop. *)
  (*   destruct IA. *)
  (*   destruct (IAAlgebra). *)
  (*   exact separator. Defined. *)
  (* Print sep. *)
  Definition IAPROP:=X.
  Definition IAEvidence:= {x :X| separator x }.
  Definition GetEvidence:IAEvidence -> X:=fun x => proj1_sig x. 
  Coercion GetEvidence: IAEvidence >-> X.
  Notation "ι( x )":=(GetEvidence x).
  
  Definition realizes:  IAPROP -> IAEvidence -> IAPROP -> Prop:=
    fun φ e θ => ι(e) ≤ arrow φ θ.

  (** IS product **)

  #[refine] Global Instance ProdOrder (I:SET): @Order _ :=
    {ord := fun (p p':I -> X) => forall i:I, ord (p i) (p' i)}.
  Proof.
    intros phi i. reflexivity.
    intros p q r H1 H2 i. transitivity (q i);[apply H1|apply H2].
  Defined.

  
(*   ∩ E = ∩ (f:I->X) = (∩f(i))_i*)

  Definition pointwise_meet_set (I:SET) (E:@Ens (I->X)): (I-> X):=
    fun i => meet_set (fun x => exists e, E e /\ e i = x).

  Definition pointwise_join_set (I:SET) (E:@Ens (I->X)): (I-> X):=
    fun i => join_set (fun x => exists e, E e /\ e i = x).
  
  
  #[refine] Global Instance ProdCL (I:SET): @CompleteLattice _ (ProdOrder I) :=
    {meet_set := pointwise_meet_set I; join_set := pointwise_join_set I}.
  Proof.
    - intros. intro i. apply meet_elim. exists y;split;trivial.
    - intros. intro i. apply meet_intro. intros x (e,(He,Hx)). subst. now apply H. 
    - intros. intro i. apply join_elim. exists y;split;trivial.
    - intros. intro i. apply join_intro. intros x (e,(He,Hx)). subst. now apply H. 
  Defined.


  
  #[refine] Global Instance ProdIS (I:SET): @ImplicativeStructure _ (ProdOrder I) (ProdCL I):=
    { arrow := fun (a b : I-> X ) (i:I) =>  arrow (a i) (b i)}.
  Proof.
    - intros a a' b H i. apply arrow_mon_l,H.
    - intros a b b' H i. apply arrow_mon_r,H.
    - intros a B. split.
      + auto_meet_intro. intro i. apply arrow_mon_r.
        apply meet_elim. exists a0;split;trivial.
      + intro i. 
        unfold Ens,Ensembles.Ensemble in B.
        unfold meet_set at 2,ProdCL,pointwise_meet_set.
        destruct (arrow_meet (a i) ((fun x : X => exists e : I -> X, B e /\ e i = x))) as [ _ H].
        eapply ord_trans;[|apply H]. 
        apply meet_intro. intros x (z,((e,(HB,He)),Hx));subst.
        apply meet_elim.  exists (fun i => arrow (a i) (e i)).
        split;trivial. exists e;split;trivial.
  Defined.
  

  (** Define S[I] unif. sep **)

  Definition USep (I:SET): (I -> X) -> Prop:=
    fun f => exists a, separator a /\ forall i, a ≤ f i.


  #[refine] Global Instance ProdIA (I:SET): @ImplicativeAlgebra _ (ProdOrder I) (ProdCL I) (ProdIS I):=
    { separator := USep I}.
  Proof.
    - intros a b (e,(He,Ha)) (f,(Hf,Hab)).
      exists (f @ e);split. apply app_closed;assumption.
      intro i. apply adjunction. eapply ord_trans;[apply Hab|].
      apply arrow_mon_l,Ha.
    - intros a b Hab (e,(He,Ha)).
      exists e;split;trivial.
      intro i. transitivity (a i);trivial.
    - exists K;split;[apply sep_K|]. intro i.
      do 2 (apply meet_intro; intros x (e,((?a,?Ha),He));subst; simpl).
      auto_meet_elim_trans.
    - exists S;split;[apply sep_S|].
      intro i.
      do 3 (apply meet_intro; intros x (e,((?a,?Ha),He));subst; simpl).
      auto_meet_elim_trans.
  Defined.
  

  
  (** Define the IA product **)

  
  Program Definition ProdIA_PHA (I:SET): PreHeytingAlgebra
    := @IA_PHA _ _ _ _ (ProdIA I).

  Notation "a ↦ b":=(arrow a b).
  Notation "[ t ] u":= (@trm_app X (t) u ) (at level 69, right associativity).
  Notation "$ n":= (@trm_bvar X n) (at level 69, right associativity).
  Notation "λ+ t":= (@lam X 0 t) (at level 69, right associativity).

  Lemma ext_E : forall I (i:I) (phi:I->Prop) t u a, (phi i -> u ≤ t ↦ a) ->
                   ext_i@ u ≤ (meet_set (fun x =>  exists c, x = meet_set (fun y : X => phi i /\ y = t ↦ c) ↦ c)) ↦ a.
  Proof.
    intros I i phi t u a Hu. do 2 (apply beta_red,adjunction).
    auto_meet_elim_risky. apply arrow_mon_l. auto_meet_intro. now apply Hu.
  Qed.
  

  Program Definition IATripos : Tripos
    := tripos (ProdIA_PHA)
              (fun A B f => pha_morphism (ProdIA_PHA B) (ProdIA_PHA A) (fun p => fun a => p (f a)) _ _ _ _ _ _)
              (fun I J f i => forallt (fun j => f (i,j)))
              (fun I J f i => existst (fun j => f (i,j)))
              (fun I phi i => meet_set (fun x => exists c, x= (meet_set ( fun y => phi i /\ y = top ↦ c)) ↦ c))
              X (fun x => x) (fun _ x => x) _ _ _ _ _ _ _ _ _ _ _ _ .
  Obligation Tactic := intro I; unfold PHARefines; simpl.
  Next Obligation. 
    intros J f p p' (q,((s,(Hs, Hs')),Hx)).
    simpl in *.
    exists (fun i => q (f i)). split.
    - exists s;split;auto.
    - intros; trivial.
  Qed.
  Next Obligation. 
    intros J f.
    pose (g:=fun j => (⊔ (fun x : X => exists e : I -> X, exists i, True /\ f i = j /\ e i = x))).
    destruct (htopi (ProdIA_PHA J) g) as (s,((a,(Ha,Hs)),Htop)).
    exists (fun i => s (f i)); split.
    - exists a;split;trivial. 
    - intro i. specialize (Htop (f i)).
      eapply ord_trans;[apply Htop|].
      apply arrow_mon_l. apply join_intro.
      intros x (e,(_,He));subst.  apply join_elim.
      exists e;exists i;repeat split;trivial.
  Qed.
  Next Obligation. 
    intros J f x1 x2.
    exists (fun i => Id);split.
    - exists Id;split;[apply sep_Id|reflexivity].
    - intro i.  apply adjunction,beta_red, meet_intro.
      intros x (e,((c,He),Hx));subst.  apply meet_elim.
      exists (fun i0 : I => arrow (arrow (x1 (f i0)) (arrow (x2 (f i0)) (c (f i0)))) (c (f i0))).
      split;trivial.  now exists (fun i => c (f i)).
  Qed.
  Next Obligation. 
    intros J f.
    pose (g:=fun j => (meet_set (fun x:X  => exists e : I -> X, exists i, True /\ f i = j /\ e i = x))).
    destruct (hbote (ProdIA_PHA J) g) as (s,((a,(Ha,Hs)),Hbot)).
    exists (fun i => s (f i)). split.
    - exists a;split;trivial. 
    - intro i. specialize (Hbot (f i)).
      eapply ord_trans;[apply Hbot|].
      apply arrow_mon_r. unfold g. apply meet_intro.
      intros x (e,(_,He));subst.   apply meet_elim.
      exists e;exists i;repeat split;trivial.
  Qed.
  Next Obligation.
    intros J f x1 x2.
    destruct (hdisjp1 (ProdIA_PHA I) (fun i => x1 (f i))  (fun i => x2 (f i)))
      as (s1,((r1,(sep_r1,Hs1)),Hd1)).
    destruct (hdisjp2 (ProdIA_PHA I)  (fun i => x1 (f i))  (fun i => x2 (f i)))
      as (s2,((r2,(sep_r2,Hs2)),Hd2)).
    exists (fun i => (λ- (fun x => x @ (λ- (fun y =>  s1 i @ y ))@ (λ- (fun z =>  s2 i @ z ))))).
    split.
    - exists  ((λ- (fun r2 : X => (λ- (fun r1 : X => (λ- (fun x : X =>  x @ (λ- (fun y : X => r1 @ y)) @ (λ- (fun z : X => r2 @ z))))))))@r2@r1).
      split.
      + repeat apply app_closed;try assumption.
        realizer (λ+ λ+ λ+ [[$0] (λ+ [$2] $0)] (λ+ [$3] $0)).
      + intro i.
        repeat (apply adjunction,beta_red).
        auto_meet_intro.
        apply adjunction,beta_red.
        repeat (apply app_mon);[reflexivity | |];
        auto_meet_intro; apply adjunction,beta_red,app_mon_l;trivial.
    - intro i.
      apply adjunction,beta_red,adj_app,adj_app.
      pose (c:=(fun (j:J) =>  hdisj (ProdIA_PHA I) (fun a : I => x1 (f a)) (fun a : I => x2 (f a)) i)).
      transitivity (((x1 ↦ c) ↦ (x2 ↦ c) ↦ c) (f i)).
      + auto_meet_elim_risky. later;reflexivity.
      + repeat apply arrow_mon.
        * apply adjunction,beta_red,adj_app,Hd1.
        * apply adjunction,beta_red,adj_app,Hd2.
        * reflexivity.
  Qed.
  Next Obligation.
    intros J f x1 x2.
    exists (fun i => Id);split.
    - exists Id;split;[apply sep_Id|reflexivity].
    - intros i;now apply adjunction,beta_red. 
  Qed.
  Next Obligation.
    split;intros x;  exists (fun i => Id);split;try (exists Id;split;[apply sep_Id|reflexivity]);
      intros i;now apply adjunction,beta_red. 
  Qed.
  Next Obligation.
    intros J H f g.
    split;intros x;  exists (fun i => Id);split;try (exists Id;split;[apply sep_Id|reflexivity]);
      intros i;now apply adjunction,beta_red. 
  Qed.
  Next Obligation.
    intros J H f pab.
    exists (fun i => Id);split;try (exists Id;split;[apply sep_Id|reflexivity]);
      intros i;now apply adjunction,beta_red. 
  Qed.
  Next Obligation.
    intros J pi pij.
    split; intros (s,((e,(sep_e,He)),Hs));
      exists (fun i => e);split;try (exists e;split;trivial;intros;reflexivity).
    - intros i.
      apply adjunction,meet_intro.
      intros x (j,Hx);subst.
      apply adjunction.
      transitivity (s (i,j));[apply He |apply Hs].
    - intros (i,j).
      transitivity (s i);trivial.
      eapply ord_trans;[apply Hs| ].
      apply arrow_mon_r.
      auto_meet_elim_risky.
  Qed.
  Next Obligation.
  exists (fun i => Id);split;try (exists Id;split;[apply sep_Id|reflexivity]);
      intros i;now apply adjunction,beta_red. 
  Qed.
  Next Obligation.
    intros J pi pij.
    split; intros (s,((e,(sep_e,He)),Hs)).
    - exists (fun i => λ- (fun x => x @ e)); split.
      + exists ((λ- (fun y => λ- (fun x => x @ y)))@e);split.
        * apply app_closed;trivial.
          realizer (λ+ λ+ [$0] $1).
        * intros i. now apply beta_red.
      + intro i. apply adjunction,beta_red,adjunction.
        transitivity ((∩ (fun i0 : J => pij (i, i0) ↦ pi i)) ↦ pi i).
        * auto_meet_elim_risky.
        * apply arrow_mon_l.
          auto_meet_intro.
          eapply ord_trans;[apply He|apply Hs].
    - exists (fun i => λ- (fun x => e @ (λ- (fun z => z @ x)))); split.
      + exists ( (λ- (fun y => λ- (fun x => y @ (λ- (fun z => z @ x)))))@e);split.
        * apply app_closed;trivial.
          realizer (λ+ λ+ [$1] (λ+ [$0] $1)).
        * intros (i,j). now apply beta_red.
      + intros (i,j).
        apply adjunction,beta_red,adjunction. 
        transitivity (s i);[apply He |]. 
        eapply ord_trans;[apply Hs| ].
        apply arrow_mon_l.
        apply type_ex_intro.
        now exists j.
  Qed.
  Next Obligation.
    intros sa sa' Hs.
    exists (fun i => Id);split;try (exists Id;split;[apply sep_Id|reflexivity]);
      intros i;apply adjunction,beta_red.
    apply meet_intro.
    intros x (c,Hc);subst.
    (* transitivity (meet_set (fun y  => sa' i /\ y = ⊤ ↦ c) ↦ c). *)
    auto_meet_elim_risky.
    apply arrow_mon_l.
    apply meet_intro.
    intros x (Hi,Hx);subst.
    apply meet_elim.
    split;auto.
  Qed.
  Next Obligation.
    intros J f phi. split;
    exists (fun i => Id);split;try (exists Id;split;[apply sep_Id|reflexivity]);
      intros i;apply adjunction,beta_red;
        apply meet_intro; intros x (c,Hc);subst;
      auto_meet_elim_risky; apply arrow_mon_l; apply meet_intro.
  Qed.
  Next Obligation.
    intros J phi f psi p H (s,((e,(sep_e,He)),Hs)).
    pose (t:= (λ-(fun e => λ-(fun x:X => x@(λ-(fun y => e@x))))) @ e).
    exists (fun i => t);split.
    -  exists t;split;[|intros;reflexivity].
       apply app_closed;trivial.
       realizer (λ+ λ+ [$0] (λ+ [$2] $1)).
    - intros j.
      apply beta_red,adjunction,beta_red,adjunction.
      transitivity (meet_set (fun y : X => psi j /\ y = ⊤ ↦ p j) ↦ p j).
      + auto_meet_elim_risky.
      + apply arrow_mon_l.
        auto_meet_intro.
        destruct (H _ a) as (i,Hi,Hj);subst.
        apply adjunction,beta_red,adjunction.
        transitivity (s i);trivial.
        eapply ord_trans;[apply Hs|].
        apply arrow_mon_l.
        auto_meet_intro.
        transitivity ( meet_set (fun y : X => psi (f i) /\ y = ⊤ ↦ a0) ↦ a0);[auto_meet_elim_risky|].
        apply arrow_mon_l.
        auto_meet_intro.
        apply meet_elim;split;trivial.
  Qed.
  Next Obligation.
    exists (fun i => (λ-(fun y => λ-(fun x => y @ x)))@ext_i);split.
    - exists ((λ-(fun y => λ-(fun x => y @ x)))@ext_i);split;[|intros;reflexivity].
      apply app_closed;[|apply ext_i_sep]. realizer (λ+ λ+ [$1] $0).
    - intros i;apply beta_red,adjunction,beta_red,beta_red.
      auto_meet_intro. apply adjunction,beta_red,adjunction.
      transitivity (top ↦ a);[apply meet_elim;split;trivial|].
      apply arrow_mon_l,top_max.
  Qed.
  Next Obligation.

    intros sa1 sa2.
    - pose (r:=λ-(fun t:X => ((ext_i @ λ-(fun y => ext_i @ ext_i)) @ ( t@proj1)) @ (t@proj2))).
    exists (fun i => r);split.
    * exists ((λ-(fun p1 =>λ-(fun p2 =>λ-(fun e => (λ-(fun t => ((e @ λ-(fun y => e @ e)) @ ( t@p1)) @ (t@p2)))))))@proj1@proj2@ext_i).
      split;[|intro;do 3 (apply adjunction);now do 3 (apply adjunction,beta_red) ].
      repeat apply app_closed.
      + pose (s:=λ+ λ+ λ+ λ+ ( [[[$1] (λ+ [$2] $2)] ([$0] $3)] ([$0] $2))).
        apply sep_translated_closed with s;[auto_closed|].
        repeat (auto_meet_intro;apply adj_imp).
        rename a into p1,a0 into p2,a1 into e,a2 into t.
        do 4 apply adj_app. do 4 try_beta. 
        do 2 (rewrite translate_app; apply app_mon;[| rewrite translate_app;reflexivity]).
        rewrite translate_app;apply app_mon;[reflexivity|].
        auto_meet_intro;apply adj_imp. apply adj_app. try_beta. reflexivity.
      + unfold proj1;realizer (λ+ λ+ $1).
      + unfold proj2;realizer (λ+ λ+ $0).
      + unfold ext_i;realizer (λ+ λ+ [$0] $1) .
    * intro i.
    pose (ex_conj:=(meet_set (fun x : X => exists e : I -> X, (exists c : I -> X, e =(fun i0 : I =>
            (meet_set (fun x0 : X => exists c0 : X, x0 = meet_set (fun y : X => sa1 i0 /\ y = ⊤ ↦ c0) ↦ c0)
          ↦ meet_set  (fun x0 : X => exists c0 : X, x0 = meet_set (fun y : X => sa2 i0 /\ y = ⊤ ↦ c0) ↦ c0) ↦ 
                   c i0) ↦ c i0)) /\ e i = x))).
    pose (ex1:= meet_set (fun x0 : X =>  exists c0 : X, x0 = meet_set (fun y : X => sa1 i /\ y = ⊤ ↦ c0) ↦ c0)).
    pose (ex2:= meet_set (fun x0 : X =>  exists c0 : X, x0 = meet_set (fun y : X => sa2 i /\ y = ⊤ ↦ c0) ↦ c0)).
    pose (conj_ex := meet_set (fun x : X => exists c, x = meet_set (fun y => (sa1 i /\ sa2 i) /\ y = ⊤ ↦ c) ↦ c)).
    unfold pointwise_meet_set;simpl. fold ex_conj. fold conj_ex.
    apply adjunction,beta_red,adjunction,adjunction.
    transitivity (ex1 ↦ ex2 ↦ conj_ex).
    + apply ext_E. intro H1. apply adjunction,beta_red,ext_E.
      intro H2. apply adjunction,beta_red.
      auto_meet_intro. apply adjunction,beta_red,adjunction.
      apply meet_elim. repeat split;trivial.
    + repeat (apply arrow_mon);[| |reflexivity].
      ++ auto_meet_intro;eapply type_proj1;auto_meet_intro;
          auto_meet_elim_risky. later. reflexivity. apply arrow_mon_l.
        repeat apply arrow_mon;try reflexivity.  auto_meet_elim_risky.
      ++ auto_meet_intro;eapply type_proj2;auto_meet_intro;
      auto_meet_elim_risky. later. reflexivity. apply arrow_mon_l.
      repeat apply arrow_mon;try reflexivity.  auto_meet_elim_risky.
  Qed.
  Next Obligation.
    intros p;split; exists (fun i => Id);split;try (exists Id;split;[apply sep_Id|reflexivity]);
      intros i;now apply adjunction,beta_red. 
  Qed.
End IATripos.

Program Definition IAT  (IA : ImplicativeAlgebraObject) : Tripos:=
  @IATripos _ _ _ _ (IAAlgebra IA).


  Program Definition IATMorphism (int : Prop) (IA1 IA2 : ImplicativeAlgebraObject) (F : ImplicativeAlgebraMorphism int IA1 IA2) : TriposMorphism int (IAT IA1) (IAT IA2)
:= tripos_morphism int (IAT IA1) (IAT IA2) (fun A f a => IAMap F (f a))  _ _ _ _ _ _ _.
Obligation Tactic := intros int IA1 IA2 F; intros;
(* pose (app1 :=@app _ _  _ (IAStructure IA1)); *)
pose (app2 :=@app _ _  _ (IAStructure IA2));
pose (abs2 :=@abs _ _  _ (IAStructure IA2));
pose (arr2 :=@arrow _ _  _ (IAStructure IA2));
pose (arr1 :=@arrow _ _  _ (IAStructure IA1));
simpl in *.
Next Obligation.
  destruct H as (s,((e,(sep_e,He)),Hs)).
  assert (H:=iamunary F e).
  pose (t:= (⊓ (fun a2 : IASet IA2 =>
            exists a1 a1' : IASet IA1,
              IAOrd IA1 e (a1 ↦ a1') /\ IAMap F a1 ↦ IAMap F a1' = a2))).
  fold t in H.  assert (H':=iamsep F).
  exists (fun a => t);split.
  - exists t;split;[now apply H|intro;reflexivity].
  - intro a. apply meet_elim_trans.
    later. split. later. later. split.
    transitivity ((s a));[apply He|apply Hs].
    reflexivity. reflexivity.
Qed.
Next Obligation. (* Proof analogous for the functoriality of uef *)
  unfold pointwise_meet_set.
  simpl.  assert (Unary:=iamunary_real F).
  destruct F;simpl in *. rename IAMap into F.
  pose (sarr:= (⊓ (fun a2 : IASet IA2 => exists a1 a1', (F a1 ↦ F a1') ↦ F (a1 ↦ a1') = a2))).
  pose (sarr':=(⊓ (fun a2  => exists a1 a1' , F (a1 ↦ a1') ↦ F a1 ↦ F a1' = a2))).
  pose (smeet:=( (⊓ (fun a2 => exists as1,
    (⊓ (fun a3 => exists2 a1, as1 a1 & F a1 = a3)) ↦ F (⊓ as1) =        a2)))).
  pose (t:= IAapp (IAapp (IAapp (IAabs (fun sarr => (IAabs (fun smeet => IAabs (fun sarr'=> IAabs (fun x => IAapp x (IAabs (fun x1 => IAabs (fun x2 => IAapp smeet (IAapp sarr(IAabs (fun x =>
             (IAapp (IAapp sarr' (IAapp (IAapp sarr' x) x1 )) x2))))))))))))) sarr) smeet) sarr').
  exists (fun i => t);split.
  + exists t;split;[|intro;reflexivity].
    repeat (apply app_closed);try assumption.
    realizer (λ[IA2]+ λ+ λ+ λ+ ([$0] (λ+ λ+ ([$4] ([$5]  (λ+ ([[$4] ([([$4] $0)] $2)] $1 ))))))).
  + intro i. (* intros p1 p2. *)
    do 2 (apply adj_app).
    do 3 (apply beta_red,adj_imp).
    apply beta_red. apply adj_app.  apply meet_elim_trans.
    pose (c:= fun i=>  F (@meet_set _ _ (IALattice IA1) (fun x : IASet IA1 =>  exists e : A -> IASet IA1, (exists c : A -> IASet IA1, e =  (fun j : A => arr1 (arr1 (pa1 j) (arr1 (pa2 j) (c j))) (c j))) /\  e i = x))).
    exists ( arr2 (arr2 (F (pa1 i)) (arr2 (F (pa2 i)) (c i))) (c i));split.
    * repeat (later;try split);reflexivity.
    *  apply arrow_mon_l.
       apply adj_imp,beta_red,adj_imp,beta_red.
       apply adj_app.
       auto_meet_elim_trans. apply arrow_mon_l. 
       auto_meet_intro. intros y (a,(d,((e,He),Hd)),Ha);subst.
       apply adj_app.
       transitivity (( F (pa1 i↦ pa2 i↦ e i) ↦ F (e i)) ↦ (F ((pa1 i↦ pa2 i↦ e i )↦  e i))).
       auto_meet_elim_risky. eexists;reflexivity.
       apply arrow_mon_l.
       apply adj_imp,beta_red,adj_app,adj_app. 
       transitivity ((F ( pa2 i ↦  e i)) ↦ ((F (pa2 i))↦ (F (e i)))).
       apply meet_elim;eexists;eexists. reflexivity.
       apply arrow_mon_l,adj_app,adj_app.
       apply meet_elim;eexists;eexists;reflexivity.
Qed.
Next Obligation.
  simpl.    destruct F;simpl in *. rename IAMap into F.
  pose (sarr:= (⊓ (fun a2 : IASet IA2 => exists a1 a1', (F a1 ↦ F a1') ↦ F (a1 ↦ a1') = a2))).
  exists (fun i => sarr);split.
  - exists sarr;split;[assumption|intro;reflexivity].
  - intro i;auto_meet_elim_risky. later. reflexivity.
Qed.  
Next Obligation.
  split;exists (fun i => @Id _ _ _ (IAStructure IA2));split;
    try (exists (@Id _ _ _ (IAStructure IA2));split;[apply sep_Id|intro;reflexivity]);
  intro i;auto_meet_elim_risky.
Qed. 
Next Obligation.
  destruct F;simpl in *. rename IAMap into F.
  pose (tmeet:=  (⊓ (fun a2 : IASet IA2 =>
                  exists as1 : IASet IA1 -> Prop,
                    (⊓ (fun a3 : IASet IA2 =>
                        exists2 a1 : IASet IA1, as1 a1 & F a1 = a3))
                      ↦ F (⊓ as1) = a2))).
  clear iamdense. fold tmeet in iammeet. unfold forallt.
  exists (fun i => tmeet);split.
  - exists tmeet;split;[assumption|intro;reflexivity].
  - intro i. apply meet_elim_trans. later;split.
    + exists (fun a => exists j:B, a = pab (i,j)). reflexivity.
    + apply arrow_mon.
      * auto_meet_intro. intros x (a1,(j,H1),Hx);subst. auto_meet_elim_risky.
      * apply by_refl. f_equal. 
Qed.
Next Obligation.
  assert (Unary_real:=iamunary_real F).
  assert (Unary:=iamunary F).
  destruct F;simpl in *. rename IAMap into F.
  pose (ext1 :=  @ext_i _ _ _ (IAStructure IA1)).
  pose (ext2 :=  @ext_i _ _ _ (IAStructure IA2)).
  pose (top1 :=  @top  _ _ (IALattice IA1)).
  pose (top2 :=  @top  _ _ (IALattice IA2)).
  pose (meet_set2 :=  @meet_set _ _ (IALattice IA2)).
  split.
  clear iamdense.
  -  pose (sarr:= (⊓ (fun a2 : IASet IA2 => exists a1 a1', (F a1 ↦ F a1') ↦ F (a1 ↦ a1') = a2))).
     pose (sarr':=(⊓ (fun a2  => exists a1 a1' , F (a1 ↦ a1') ↦ F a1 ↦ F a1' = a2))).
     pose (smeet:=( (⊓ (fun a2 => exists as1,
                            (⊓ (fun a3 => exists2 a1, as1 a1 & F a1 = a3)) ↦ F (⊓ as1) =  a2)))).
     pose (smeet':=  (⊓ (fun a2 : IASet IA2 =>  exists as1 : IASet IA1 -> Prop,
                             F (⊓ as1) ↦ (⊓ (fun a3 =>  exists2 a1, as1 a1 & F a1 = a3))= a2))).
    assert (H0: exists s,
    IASeparator IA2 s /\
    (forall i, @ord _ (IAOrder IA2) s
     (arr2 (F
            (⊓
               (fun x : IASet IA1 =>
                exists c : IASet IA1,
                  x =
                  arr1 (⊓ (fun y : IASet IA1 => sa i /\ y = arr1 top1 c)) c)))
         (⊓
            (fun x : IASet IA2 =>
             exists c, x = arr2 (⊓ (fun y : IASet IA2 => sa i /\ y = arr2 (F top1) (F c))) (F c)))))).
     1:{ exists (abs2 (fun x => (abs2 (fun y => app2 (app2 sarr' (app2 smeet' x)) (app2 smeet (app2 sarr y))))));split.
       admit.
       intro i. apply adjunction,beta_red.
       auto_meet_intro. apply adjunction,beta_red.
       apply adjunction.
       transitivity ((⊓ (fun x => exists c, x = arr2 (F(⊓ (fun y : IASet IA1 => sa i /\ y = arr1 top1 c))) (F c)))).
       * auto_meet_intro.  apply adjunction. apply meet_elim_trans.
         eexists. split. eexists. eexists. reflexivity.
         apply arrow_mon_l. apply adjunction.
         apply meet_elim_trans. eexists. split.  eexists. reflexivity.
         apply arrow_mon_r. apply meet_elim. eexists. eexists. reflexivity. reflexivity.
       * apply meet_elim_trans. eexists. split. eexists. reflexivity.
         apply arrow_mon_l,adjunction.
         apply meet_elim_trans. eexists. split. eexists. reflexivity.
         apply arrow_mon_l. auto_meet_intro. intros x (a1,(Hi,Ha1),Hx);subst. apply adjunction.
         apply meet_elim_trans. eexists. split. do 2 eexists. reflexivity.
         apply arrow_mon_l. apply meet_elim. split;trivial.
     }
    assert (G1: exists s,
    IASeparator IA2 s /\
    (forall i, @ord _ (IAOrder IA2) s
               (arr2          (⊓
            (fun x : IASet IA2 =>
             exists c, x = arr2 (⊓ (fun y : IASet IA2 => sa i /\ y = arr2 (F top1) (F c))) (F c)))
         ( F(@join_set _ _ (IALattice IA1)  (fun x  => sa i /\ x = top1)))))).
     1:{
       specialize (Unary _ (sep_Id)).
       pose (r:= (⊓ (fun a2 : IASet IA2 =>
                exists a1 a1' : IASet IA1,
                  IAOrd IA1 (@Id _ _ _ (IAStructure IA1)) (a1 ↦ a1') /\ F a1 ↦ F a1' = a2))).
       exists (app2 ext2 r );split.
       + admit.
       + intro i.
         apply beta_red,adjunction,beta_red,adjunction.
         apply meet_elim_trans.
         eexists; split;[eexists;reflexivity|].
         apply arrow_mon_l.
         apply meet_intro. intros ? [? ?];subst.
         apply meet_elim_trans.
         eexists; split;[repeat eexists|].  apply adjunction,beta_red. reflexivity.
         apply arrow_mon_l.
       exists (abs2 (fun x => app2 x  (abs2 (fun y => y))));split.
       + admit.
       + intro i.
         apply adjunction,beta_red,adjunction.
         apply meet_elim_trans.
         eexists; split;[later;reflexivity|].
         apply arrow_mon.
         apply meet_intro. intros ? [? ?];subst.
         apply adjunction,beta_red. reflexivity.
         
     }
   assert (H1: exists s,
    IASeparator IA2 s /\
    (forall i, @ord _ (IAOrder IA2) s
               (arr2          (⊓
            (fun x : IASet IA2 =>
             exists c, x = arr2 (⊓ (fun y : IASet IA2 => sa i /\ y = arr2 (F top1) (F c))) (F c)))
         (⊓
            (fun x : IASet IA2 =>
             exists c : IASet IA2,
               x = arr2 (⊓ (fun y : IASet IA2 => sa i /\ y = arr2 (F top1) c)) c))))).
     1:{
  (*      assert ext_EF : forall t u a, (sa i -> u ≤ t ↦ F a) -> *)
  (*                  ext_i@ u ≤ (meet_set (fun x =>  exists c, x = meet_set (fun y : X => phi i /\ y = t ↦ (F c)) ↦(F c)) ↦ a. *)
  (* Proof. intros I i phi t u a Hu. do 2 (apply beta_red,adjunction). auto_meet_elim_risky. apply arrow_mon_l. auto_meet_intro. now apply Hu. Qed. *)

  (*      exists (app2 ext2 ext2);split. *)
  (*      * apply app_closed; apply ext_i_sep. *)
  (*      * intro i. *)
  (*        do 2 (apply beta_red,adjunction). auto_meet_elim_risky. apply arrow_mon_l. auto_meet_intro. *)
  (*        apply ext_E. intro Hi. *)
         admit.
    }
    assert (H2: exists s,
    IASeparator IA2 s /\
    (forall i, @ord _ (IAOrder IA2) s
     (arr2 ((⊓
               (fun x =>exists c,  x = arr2 (⊓ (fun y  => sa i /\ y = arr2 (F top1) c)) c)))
         (⊓ (fun x : IASet IA2 =>
             exists c : IASet IA2,
               x = arr2 (⊓ (fun y : IASet IA2 => sa i /\ y = arr2 top2 c)) c))))).
    1:{  exists (app2 ext2 ext2);split.
       * apply app_closed; apply ext_i_sep.
       * intro i. apply ext_E. intro Hi.
         transitivity ( top2 ↦ (⊓ (fun x : IASet IA2 =>
                                     exists c : IASet IA2,
                                       x = arr2 (⊓ (fun y : IASet IA2 => sa i /\ y = arr2 top2 c)) c))).
         apply adjunction,beta_red. auto_meet_intro.
         apply adjunction,beta_red,meet_elim,meet_elim. now split.
         apply arrow_mon_l,top_max.}
    destruct H0 as (s0,(sep_s0,H0)).
     destruct H1 as (s1,(sep_s1,H1)).
     destruct H2 as (s2,(sep_s2,H2)).
    exists (fun i => app2 (app2 (app2 (abs2 (fun w =>  (abs2 (fun y =>  (abs2 (fun z =>  (abs2 (fun x => app2 y (app2 z (app2 w x)))))))))) s0) s2) s1);split.
    ++ exists (app2 (app2 (app2 (abs2 (fun w =>  (abs2 (fun y =>  (abs2 (fun z =>  (abs2 (fun x => app2 y (app2 z (app2 w x)))))))))) s0) s2) s1). split.
      *  repeat apply app_closed;try assumption. unfold abs2,app2.
         realizer  (λ[IA2]+ λ+ λ+ λ+ [$2] ([$1] ([$3] $0))).
      * intro;reflexivity.
        ++ intro i.
           do 3 apply adj_app. 
           do 4 apply adjunction,beta_red.
           apply adjunction. eapply ord_trans;[apply H2|]. apply arrow_mon_l.
           apply adjunction. eapply ord_trans;[apply H1|]. apply arrow_mon_l.
           apply adjunction. eapply ord_trans;[apply H0|]. apply arrow_mon_l.
      reflexivity.
      
    (* + admit. *)
    (* + intro i. *)
    (*   apply adjunction,beta_red,adjunction. *)
    (*   pose (conc:=meet_set2  (fun x => exists c, x = arr2 (meet_set2 (fun y => sa i /\ y = arr2 (@top _ _ (IALattice IA2)) (F c))) (F c))).  *)
    (*   transitivity (arr2 (meet_set2 (fun x => exists c, x = arr2 ( meet_set2 (fun y  => sa i /\ y = arr2 (F(@top _ _ (IALattice IA1))) c)) c)) conc ). *)
    (*   (* transitivity ((meet_set2 (fun x => exists c, x = (meet_set2 (fun y  => sa i /\ y = arr2 (F(@top _ _ (IALattice IA1))) (F c))) ↦ (F c))) ↦ conc ). *) *)
    (*   eapply ord_trans. *)
    (*   * apply (ext_EF _ i phi _ _ . *)
    (*     intro H1. eapply ord_trans;[|apply arrow_mon_l,(@top_max _ _ (IALattice IA2))]. *)
    (*     apply adjunction,beta_red. auto_meet_intro. apply adjunction,beta_red,adjunction. *)
    (*     apply meet_elim;split;trivial. *)
    (*   * apply arrow_mon_l. repeat  auto_meet_intro. *)
  - assert (H:=  iambinary _ (sep_Id)).
    pose (s:=⊓ (fun a2 : IASet IA2 =>
            exists a1 a1' a1'' : IASet IA1,
              IAOrd IA1 (@Id _ _ _ (IAStructure IA1)) (a1 ↦ a1' ↦ a1'') /\ F a1 ↦ F a1' ↦ F a1'' = a2)).
    exists (fun i => app2 ext2 (abs2 (fun x => app2 (app2 s (F ext1)) (F top1))));split.
    + exists (app2 ext2 (app2 (app2 (app2 (abs2 (fun z => abs2 (fun y => (abs2 (fun s => (abs2 (fun x => app2 (app2 s z) y))))))) (F ext1)) (F top1)) s)).
      split.
      * repeat apply app_closed;
          [apply ext_i_sep| | apply iamsep,ext_i_sep| eapply iamsep,up_closed;[apply top_max|apply sep_Id]|apply H].
        unfold abs2,app2.
        realizer (λ[IA2]+ λ+ λ+ λ+ [[$1] $3] $2).
      * intro a. apply app_mon_r. do 3 apply adj_app. now do 3 apply adjunction,beta_red.
    + intro i. apply ext_E. intro Hi.
      apply adjunction,beta_red,adj_app,adj_app.
      apply meet_elim. repeat eexists.
      do 2 apply adjunction,beta_red. auto_meet_intro. apply adjunction,beta_red.
      now apply adjunction,meet_elim.
Qed.
Next Obligation.
  destruct F. rename IAMap into F. simpl in *.
  destruct iamdense as (f,(Hf1,Hf2),Hfint).
  exists f. split.
  - pose (r:=(⊓ (fun a2 : IASet IA2 => exists a2' : IASet IA2, a2' ↦ F (f a2') = a2))).
    exists (fun i => r);split;[exists r;split;try assumption;intro i;reflexivity|].
    intro i. apply meet_elim.  now exists i.
  - pose (r:=(⊓ (fun a2 : IASet IA2 => exists a2' : IASet IA2, F (f a2') ↦ a2' = a2))).
    exists (fun i => r);split;[exists r;split;try assumption;intro i;reflexivity|].
    intro i. apply meet_elim.  now exists i.
  - intros p.                                    
    destruct (Hfint p) as (finv,H1,H2).
    now exists finv.
Qed.



Program Definition IATFunctor int: Pseudofunctor (ImplicativeAlgebraCategory int) (TriposCategory int)
:= pseudofunctor (ImplicativeAlgebraCategory int) (TriposCategory int) IAT (@IATMorphism int) _ _ _.
Obligation Tactic := intro int; unfold Equiv; simpl (Refines _);unfold TriposRefines; simpl.
Next Obligation.
Admitted.
Next Obligation.
Admitted.
Next Obligation.
Admitted.


(** ** Natural Transformation:  UFam o UEF => IAT*)
Require Import EF.EFIAEquivalence.
Require Import EF.EFTriposEquivalence.


Check (fun int => pseudonatural_transformation (cat_comp (UEFFunctor int) (UFamFunctor int)) (IATFunctor int)).

Check tripos_morphism.
Program Definition ufam_uef_to_iat (int:Prop): PseudonaturalTransformation (cat_comp (UEFFunctor int) (UFamFunctor int)) (IATFunctor int) :=
  pseudonatural_transformation (cat_comp (UEFFunctor int) (UFamFunctor int)) (IATFunctor int)
                               (fun IA => tripos_morphism int (UFam (uef IA)) (IAT  IA) (fun _ x => x) _ _ _ _ _ _ _)
                               _ .
Obligation Tactic:=intros.
Next Obligation.
  simpl.
  
  Print TriposCategory.
Admitted.
Next Obligation.
Admitted.
